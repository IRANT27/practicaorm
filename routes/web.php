<?php

use App\Models\User;
use App\Models\Publish;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('users', function () {
    return User::get();
});

Route::get('users/create', function () {
	$new_user = User::create([
		 'first_name' => "Juan",
        "last_name"  => "Gonzales",
        'email'  => "nada@nada.com",
        'pasword'  => "123456"
	]);
//dd($new_user);
    return $new_user;
});

Route::get('users/{user_id}', function ($user_id) {
    return User::find($user_id);
});
// devolver las publicaciones que si estan publicadas que tienen la variable is_publish en true mediante consulta a la base de datos
Route::get('is_publish', function () {
    return Publish::where("is_publish","=","1")->get();
});

Route::get('is_publish2', function () {
    return Publish::available()->get();

    return 
    Publish::get()->filter(function($avaliable){
        return $avaliable->is_available;
    });
    
});