<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Publish extends Model
{
    protected $table = "publishes";

    protected $available  = 1; 


    public function getIsAvaliableAttribute()
    {
        return (boolean) ($this->available = 1);
    }


    public function scopeAvailable($query)
    {
        return $query->where("is_publish","=",$this->available);
    }

}
