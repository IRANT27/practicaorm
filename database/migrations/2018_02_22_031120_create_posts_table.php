<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id',10);

            $table->unsignedinteger('autor_id')->nullable();
            $table->foreign('autor_id')->references('id')->on('users');

            $table->unsignedinteger('publishes_id')->nullable();
            $table->foreign('publishes_id')->references('id')->on('publishes');
            //$table->timestamps('publish_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
