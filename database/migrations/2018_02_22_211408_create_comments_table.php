<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id',10);

            $table->unsignedinteger('autor_id')->nullable();
            $table->foreign('autor_id')->references('id')->on('users');

            $table->text('comment');

            $table->timestamps();

            $table->unsignedinteger('posts_id')->nullable();
            $table->foreign('posts_id')->references('id')->on('posts');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
