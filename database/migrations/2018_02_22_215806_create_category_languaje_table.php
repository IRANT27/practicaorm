<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryLanguajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_laguaje', function (Blueprint $table) {
            $table->unsignedinteger('categories_id')->nullable();
            $table->foreign('categories_id')->references('id')->on('categories');

            $table->unsignedinteger('languaje_id')->nullable();
            $table->foreign('languaje_id')->references('id')->on('languajes');

            $table->string('label',255);
            $table->string('slug',255);
            $table->text('description');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_languaje');
    }
}
