<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguajePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languaje_post', function (Blueprint $table) {
            $table->unsignedinteger('posts_id')->nullable();
            $table->foreign('posts_id')->references('id')->on('posts');

            $table->unsignedinteger('languaje_id')->nullable();
            $table->foreign('languaje_id')->references('id')->on('languajes');

            $table->string('title',255);
            $table->string('slug',255);
            $table->text('content');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laguaje_post');
    }
}
