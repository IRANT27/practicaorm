<?php

use Illuminate\Database\Seeder;
use App\Models\Publish;

class PublishesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Publish::class,50)->create();
    }
}
