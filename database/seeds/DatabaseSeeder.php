<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PublishesTableSeeder::class);
        $this->call(PhotosTableSeeder::class);
        $this->call(languajesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
    }
}
