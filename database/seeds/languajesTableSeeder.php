<?php

use Illuminate\Database\Seeder;
use App\Models\Languaje;

class languajesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Languaje::class,5)->create();
    }
}
