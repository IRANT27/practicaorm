<?php
use Faker\Generator as Faker;
use App\Models\Publish;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Publish::class, function (Faker $faker) {
    return [
        'slug'          => $faker->slug,
        'label'  => $faker->text($maxNbChars = 200)  ,
        'is_publish'  => rand(0,1),
    ];
});
