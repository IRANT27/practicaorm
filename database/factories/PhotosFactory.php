<?php
use Faker\Generator as Faker;
use App\Models\Photo;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Photo::class, function (Faker $faker) {
    return [
        'filename'          => $faker->mimeType ,
        'type'  => rand(1,5)  ,
        
    ];
});
